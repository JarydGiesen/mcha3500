#include "tm_stm32_mpu6050.h"
#include "math.h"

//variable declarations
TM_MPU6050_t IMU_datastruct;
TM_MPU6050_Result_t teste;


//function definitions
void IMU_init(void)
{
	TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}


void IMU_read(void)
{
	TM_MPU6050_ReadAll(&IMU_datastruct);
}


float get_accY(void)
{

	float YaccRead = IMU_datastruct.Accelerometer_Y;
	float YaccConv = YaccRead / ((32767 + 10) / (9.81 * 4));

	return YaccConv;
}


float get_accZ(void)
{

	float ZaccRead = IMU_datastruct.Accelerometer_Z;
	float ZaccConv = ZaccRead / ((32767) / (9.81 * 4));

	return ZaccConv;
}


float get_gyroX(void)
{

	float XgyRead = IMU_datastruct.Gyroscope_X;
	float XgyConv = (XgyRead / ((32767 + 9.5) / 250)) / 57.3;

	return XgyConv;
}

double get_acc_angle(void)
{
	float Ya = get_accY();
	float Za = get_accZ();

	double theta_acc = -atan2(Za,Ya);

	return theta_acc;
}