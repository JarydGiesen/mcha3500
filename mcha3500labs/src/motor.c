#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"


#include "motor.h"


static uint32_t enc_count = 0;


static GPIO_InitTypeDef GPIO_InitStruct;
static GPIO_InitTypeDef GPIO_InitStruct2;

static TIM_HandleTypeDef _htim3;
static TIM_OC_InitTypeDef _sConfigPWM;

void motor_PWM_init(void)
{


	// Enable TIM3 and GPIOA clocks
	__HAL_RCC_TIM3_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();

	// initialise PA6
	GPIO_InitStruct.Pin = GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;

	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	// Initialising the timer 3 stuffs
	_htim3.Instance =           TIM3;
    _htim3.Init.Prescaler =     TIM_CLOCKPRESCALER_DIV1;
    _htim3.Init.CounterMode =   TIM_COUNTERMODE_UP;
    _htim3.Init.Period =        10000;
    _htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;




    // config timer 3  channel
    _sConfigPWM.OCMode =     TIM_OCMODE_PWM1;
    _sConfigPWM.Pulse =      0;
    _sConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH;
    _sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;

	//initialise pwm tim3
	HAL_TIM_PWM_Init(&_htim3);

	//config channel 1 corresponding to pc6
	HAL_TIM_PWM_ConfigChannel(&_htim3, &_sConfigPWM, TIM_CHANNEL_1);

	//set ccr to 25% initially
    __HAL_TIM_SET_COMPARE(&_htim3, TIM_CHANNEL_1, 2500);

	//start our pwm finally
	HAL_TIM_PWM_Start(&_htim3, TIM_CHANNEL_1);

}



void motor_encoder_init(void)
{


	// Enable GPIOC clocks
	__HAL_RCC_GPIOC_CLK_ENABLE();

	//initialising PC0 and PC1
	GPIO_InitStruct2.Pin = GPIO_PIN_0|GPIO_PIN_1;
	GPIO_InitStruct2.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct2.Pull = GPIO_NOPULL;
	GPIO_InitStruct2.Speed = GPIO_SPEED_FREQ_HIGH;

	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct2);


	HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0F, 0x0F);
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0F, 0x0F);


	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);


}




void EXTI0_IRQHandler(void)
{

	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	{
		enc_count++;
	}

	else
	{
		enc_count--;
	}


    /* Call the HAL GPIO EXTI IRQ Handler and specify the GPIO pin */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}


void EXTI1_IRQHandler(void)
{

	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	{
		enc_count--;
	}

	else
	{
		enc_count++;
	}

    /* Call the HAL GPIO EXTI IRQ Handler and specify the GPIO pin */
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}


uint32_t motor_encoder_getValue(void)
{

	return enc_count;

}