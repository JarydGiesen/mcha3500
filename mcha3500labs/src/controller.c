#include <stddef.h>
#include "stm32f4xx_hal.h"
#include "arm_math.h"
#include "controller.h"

static float ctrl_mK_f32[5] =
{
	//negative K matrix 1x5
	57.4827f, 162.0246f, 49.4904f, 26.5967f, 29.8511f,
};
static float ctrl_x_f32[5] =
{
	//estimate of state 5x1
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};
static float ctrl_u_f32[1] =
{
	//estimate of state 1x1
	0.0,
};
static float ctrl_Az_f32[5] =
{
	//state transition matrix 1x5
	0.005, 0.0, 0.0, 0.0, 1,
};
static float ctrl_z_f32[1] =
{
	//Integrator state 1x1
	0.0,
};

// Define control matrix variables
// rows, columns, data array
arm_matrix_instance_f32 ctrl_mk = {1, 5, (float32_t *)ctrl_mK_f32};
arm_matrix_instance_f32 ctrl_x = {5, 1, (float32_t *)ctrl_x_f32};
arm_matrix_instance_f32 ctrl_u = {1, 1, (float32_t *)ctrl_u_f32};
arm_matrix_instance_f32 ctrl_Az = {1, 5, (float32_t *)ctrl_Az_f32};
arm_matrix_instance_f32 ctrl_z = {1, 1, (float32_t *)ctrl_z_f32};

void ctrl_init(void)
{
	arm_mat_init_f32(&ctrl_mk, 1, 5, (float32_t *)ctrl_mK_f32);
	arm_mat_init_f32(&ctrl_x, 5, 1, (float32_t *)ctrl_x_f32);
	arm_mat_init_f32(&ctrl_u, 1, 1, (float32_t *)ctrl_u_f32);
	arm_mat_init_f32(&ctrl_Az, 1, 5, (float32_t *)ctrl_Az_f32);
	arm_mat_init_f32(&ctrl_z, 1, 1, (float32_t *)ctrl_z_f32);	
}

// functions to update vector elements
void ctrl_set_x1(float x1)
{
	//update state of the thingo
	ctrl_x_f32[0] = x1;
}
void ctrl_set_x2(float x2)
{
	//update state of the thingo
	ctrl_x_f32[1] = x2;
}
void ctrl_set_x3(float x3)
{
	//update state of the thingo
	ctrl_x_f32[2] = x3;
}
void ctrl_set_x4(float x4)
{
	//update state of the thingo
	ctrl_x_f32[3] = x4;
}

//function to get current control output
float getControl(void)
{
	return ctrl_u_f32[0];
}


//update control output
void ctrl_update(void)
{
	//compute control action
	arm_mat_mult_f32(&ctrl_mk, &ctrl_x, &ctrl_u);

	//update integrator state
	arm_mat_mult_f32(&ctrl_Az, &ctrl_x, &ctrl_z);

	// store updated value in state vector
	ctrl_x_f32[4] = ctrl_z_f32[0];
}









