#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"

#include "pendulum.h"


static GPIO_InitTypeDef GPIO_InitStruct;
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;



void pendulum_init(void)
{

	//initialise ADC1 and GPIOB clocks
	__HAL_RCC_ADC1_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	//initialise PB0
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	// Initialising ADC1
	    hadc1.Instance = ADC1;
	    hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
	    hadc1.Init.Resolution = ADC_RESOLUTION_12B;
	    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	    hadc1.Init.ContinuousConvMode = DISABLE;
	    hadc1.Init.NbrOfConversion = 1;   

	    HAL_ADC_Init(&hadc1);


        /* Configure the ADC channel 8 to sequence rank 1 */  
        sConfigADC.Channel = ADC_CHANNEL_8;
        sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
        sConfigADC.Offset = 0;
        sConfigADC.Rank = 1;
        HAL_ADC_ConfigChannel(&hadc1, &sConfigADC);


}


float pendulum_read_voltage(void)
{

	/* Start the ADC */
	HAL_ADC_Start(&hadc1);

	/* Poll and measure voltage (rank 1) */
	HAL_ADC_PollForConversion(&hadc1, 0xFF);

	// Get adc value
	float result = HAL_ADC_GetValue(&hadc1);

	// Stop the acadaca
	HAL_ADC_Stop(&hadc1);


	// calculate the adc
	float voltage = ((result * 3.3) / 4095);


	return voltage;

}