#ifndef MOTOR_H
#define MOTOR_H

/* Add function prototypes here */
void motor_PWM_init(void);
void motor_encoder_init(void);
uint32_t motor_encoder_getValue(void);


#endif