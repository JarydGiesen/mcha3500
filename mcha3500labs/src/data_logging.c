#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"

#include "data_logging.h"
#include "pendulum.h"
#include "data_logging.h"
#include "IMU.h"

uint16_t logCount = 0;


static void (*log_function)(void);

static osTimerId_t logTimerId;
static osTimerAttr_t logTimerAttr =
{
	.name = "logTimer"
};


//declate our functions
static void log_pendulum(void *argument);
static void log_imu(void);


static void log_pointer(void *argument);


//function definitions
static void log_pendulum(void *argument)
{
	//supress unused
	UNUSED(argument);

	//read voltage
    float voltageread = pendulum_read_voltage();

    //calculate time
    float temporaryConvertTime = logCount;
    float timeCount = temporaryConvertTime / 200;

    //print time count and read voltage
    printf("%f,%f\n", timeCount, voltageread);

    //increase count
    logCount = logCount + 1;

    //stop after 2 seconds
    if(timeCount >= 2)
    {
    	logging_stop();
    }

}


void logging_init(void)
{
	logTimerId = osTimerNew (log_pointer, osTimerPeriodic, NULL, &logTimerAttr);
}


static void log_pointer(void *argument)
{
    UNUSED(argument);

    (*log_function)();
}


void pend_logging_start(void)
{

    log_function = &log_pendulum;


 	logCount = 0;

 	osTimerStart(logTimerId, 5);
}


void logging_stop(void)
{
 	osTimerStop(logTimerId);
}


void imu_logging_start(void)
{
    log_function = &log_imu;

    logCount = 0;

    osTimerStart(logTimerId, 5);

}

static void log_imu(void)
{
    IMU_read();

    double Tacca = get_acc_angle()* 57.3;
    float Xgya = get_gyroX();
    float voltageread = pendulum_read_voltage();


    float temporaryConvertTime = logCount;
    float timeCount = temporaryConvertTime / 200;

    printf("%f,%f,%f,%f\n", timeCount, Tacca, Xgya, voltageread);


    logCount = logCount + 1;


    if(timeCount >= 5)
    {
        logging_stop();
    }

}